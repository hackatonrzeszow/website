var express = require('express');
var router = express.Router();
var passport=require('passport');
var user=require('../models/user');
var credit=require('../models/credit_card');
var Order=require('../models/order');
var comment=require('../models/comment');
var message=require('../models/message');
var stripe=require('stripe')("sk_test_130jgUegAGBZJUVx1gsxhldo");

var googleMapsClient=require('@google/maps').createClient({
	key: 'AIzaSyCnOrzB7Std2HPs3zgHIMS4frn20AevGgM'
});
var foursquare = (require('foursquarevenues'))('GZ1BZPHQQGMYLVYXB0CCCUTDQGW2PPEQMOKNINL15O5UMWTU', '0AMEUWUK53MT3FFJRKGCMOXXYOOCIZTUJIRVX01LJUEPYPAP');
/*	LOGIN ROUTING	*/
router.get('/login', function(req,res,next){
	var messages=req.flash('error');
    res.render('users/login', {messages:messages, hasErrors: messages.length>0});
});
router.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));
router.get('/m',function(req,res,next){
	res.render('/m');
})
router.get('/auth/google/callback', passport.authenticate('google', {  
  successRedirect: '/',
  failureRedirect: '/login',
  failureFlash:true
}));
router.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));

router.get('/auth/facebook/callback', passport.authenticate('facebook', {  
  successRedirect:'/',
  failureRedirect: '/login',
}));
router.get('/logout', function(req,res,next){
	req.logout();
	res.redirect('/');
});
//	PRIVATE PROFILE GET
router.get('/profile', function(req,res,next){
	if(req.isAuthenticated()){

    user.findById(req.user,function (err,user) {
    	if(req.user.google.id!=null){
			var accepted;
			var accbyYou;
			Order.find({userId:req.user._id, taken:true},function(err,result){
				accepted=result;
				Order.find({takenBy:req.user._id}, function(err,result){
					accbyYou=result;
					comment.find({commentedUser:req.user._id}, function(err,result)
					{
						console.log(result);
						res.render('users/profile', {platform:'Google',id:req.user._id,email:req.user.google.email, name:req.user.google.name, array:accepted, byYou:accbyYou, owns:true, comments:result, image:req.user.google.image})
					})
				})
			})			
		}
    	if(req.user.facebook.id!=null){
    		res.render('users/profile', {platform:'Facebook',email:req.user.facebook.email, name:req.user.facebook.name})
    	}
    });	
}
});
//PRIVATE MESSAGES SYSTEM
router.get('/message/:user', function(req,res,next)
{
	user.findById(req.params.user, function(err,result){
		if(result.google.id!=null){
			if(err)
			{
				res.render('error');
			}
			res.render('msg', {user:result.google.name, userID:req.params.user});
		}
	});
	//var msg=new message();
	//
	
});
router.post('/message',function(req,res,next){
	console.log(req.body.user);
	var msg=new message();
	if(req.user.google.name!=null){
		msg.from=req.user.google.name;
	} else {
		msg.from=req.user.facebook.name;
	}
	msg.to=req.body.user;
	msg.title=req.body.title;
	msg.content=req.body.context;
	msg.date=Date.now();
	msg.save(function(err,result){
		res.redirect('/');
	})
})
//SEE PRIVATE MESSAGES
router.get('/messagebox', function(req,res,next)
{
	if(req.user.google.id!=null)
	{
		message.find({to:req.user.google.name}, function(err,result){
			res.render('msgbox',{messages:result});
		})
	} else {
		message.find({to:req.user.facebook.name}, function(err,result){
			res.render('msgbox',{messages:result});
		})
	}
	
})
//SHOW SPECIFIC MESSAGE
router.get('/showmsg/:id', function(req,res,next){
	message.findById(req.params.id, function(err,result){
		result.read=true;
		result.save(function(err,result){
			res.render('spec', {message:result});
		})
		
	});
});
//PLACE COMMENT
router.post('/comment/:id',function(req,res,next){
	console.log(req.body.comment);
	var com=new comment();
	com.user=req.user._id;
	com.commentedUser=req.params.id;
	com.comment=req.body.comment;
	com.save(function(err,result){
		res.redirect('/');
	});
	
});
//	MAIN PAGE
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express'});
});
//	VERIFY BY CREDIT CARD
router.get('/checkout', function(req,res,next){
	res.render('verify');
})
router.post('/checkout',function(req,res,next){
		var stripe=require('stripe')("sk_test_130jgUegAGBZJUVx1gsxhldo");
		var ccsave=new credit();
		ccsave.user=req.user._id;
		stripe.customers.create({
			source:req.body.stripeToken
			}, function(err, customer)
				{
					console.log(customer);
					if(err)
					{
						console.log(err);
					}
					//stripe.accounts.create({
  					//country: req.body.country,
					//type: "custom"
					//}).then(function(acct) {
						ccsave.token=customer.id;
						ccsave.save(function(err, result) {
							user.findById(req.user._id,function (err,user) {
								user.verified=true;
								user.save();
							});
					//});
					
				});
				
		});
		if(req.session.oldUrl){
			res.redirect(req.session.oldUrl);
		}
		res.redirect('/');
})
// DELIVERER SIDE
router.get('/deliver', isVerified, function(req,res,next){
	Order.find({taken:false}, function(err,result){
		console.log(result);
		res.render('delivery', {array:result});
	});	
});
//ACCEPT ORDER ROUTE
router.get('/a/:id',isVerified,function(req,res,next){
	var id=req.params.id;
	Order.findById(id, function(err, order){
		order.taken=true;
		order.takenBy=req.user._id;
		order.save();
	});
	req.flash('messages', 'Delivery request accepted');
	res.redirect('/gps/'+req.params.id);
});
//PLACER SIDE
router.get('/order', function(req,res,next){
	res.render('order', {secTime:false});
});
router.post('/sendorder', function(req,res,next){
		console.log(JSON.stringify(req.body.shops));
		if(req.isAuthenticated())
		{
			if(req.user.verified)
				{
					credit.findOne({user: req.user._id}, function(err,credit)
					{
						console.log(req.user._id);
						var username;
						if(req.user.google.id!=null)
						{
							username=req.user.google.name;
						} else {
							username=req.user.facebook.name;
						}
						if(err){
							return res.write('Error')
						}
						stripe.charges.create({
							amount:100*req.body.price,
							currency:'pln',
							customer:credit.token,
							description:'Payment for services'
						});
						var saveOrder=new Order({
							userId:req.user._id,
							user:username,
							userLat:req.body.userLat,
							userLng:req.body.userLng,
							shoppingList:req.body.shops,
							cost:req.body.price
						});
						saveOrder.save(function(err,result){
							if(err) console.log(err);
							res.statusCode=200;
							res.send('/');
						});
				})
			} else {
			res.statusCode=200;
			res.send('/checkout');
			}
		} else {
			res.statusCode=200;
			res.send('/login');
		}
		
		

		
});

router.post('/placeorder', function(req,res,next){
	googleMapsClient.geocode({
  		address: req.body.place
	}, function(err, response) {
	  	if (!err) {
		  		var arr=Array();
		  		var params={
		  			'll': response.json.results[0].geometry.location.lat+","+response.json.results[0].geometry.location.lng
		  		}
		  		/*foursquare.getVenues(params, function(error, venues) {
		        	if (!error) {
		            	for(var i=0; i<venues.response.venues.length; i++)
		  				{
		  					arr.push(venues.response.venues[i].name);
		  				}
		        	}
		    	});*/
		    	googleMapsClient.placesNearby({
		  			location:{lat: response.json.results[0].geometry.location.lat, lng:response.json.results[0].geometry.location.lng},
		  			radius:50000,
		  			type:"convenience_store"
		  		}, function(err, response){
		  			if(!err)
		  			{
		  				console.log(response.json.results[0].geometry);
		  				for(var i=0; i<response.json.results.length; i++)
		  				{
		  					arr.push({	shopName:response.json.results[i].name + ", " + response.json.results[i].vicinity,
		  								lat:response.json.results[i].geometry.location.lat,
		  								lng:response.json.results[i].geometry.location.lng
		  							});
		  				}
		  				console.log(arr);
		  			}
		  		});
		  		googleMapsClient.placesNearby({
		  			location:{lat: response.json.results[0].geometry.location.lat, lng:response.json.results[0].geometry.location.lng},
		  			radius:50000,
		  			type:"store"
		  		}, function(err, response){
		  			if(!err)
		  			{
		  				for(var i=0; i<response.json.results.length; i++)
		  				{
		  					arr.push({	shopName:response.json.results[i].name + ", " + response.json.results[i].vicinity,
		  							  	lat:response.json.results[i].geometry.location.lat,
		  								lng:response.json.results[i].geometry.location.lng
		  					});
		  				}
		  				console.log(arr);
		  				res.render('order', {hasErrors:false, latitude:response.json.results[0].geometry.location.lat, longitude:response.json.results[0].geometry.location.lng, secTime:true,array:arr});
		  			}
		  		});	
	  	}
	});
});
//USER RATING
router.get('/rate/:id/:rate', function(req,res,next){
	var userId=req.params.id;
	var rate=parseInt(req.params.rate);
	console.log(rate);
	user.findById(userId, function(err,user){
		if(!err){
				user.ratingNum++;
				user.starRating=(user.starRating+rate)/user.ratingNum;				
				user.save();
				res.redirect('/');
		}
	})
});
//PROFILE SHOW
router.get('/u/:id',function(req,res,next){
	var userId=req.params.id;
	console.log(userId);
	user.findById(userId, function(err,user){
		if(!err){
			if(user.google.id!=null){
				res.render('users/profile', {platform:'Google',email:user.google.email, name:user.google.name, owns:false, id:req.params.id});
			};
		}
		else{
			console.log(err);
			res.render('/');
		}
		
	});
});
//MOBILE GPS FUNCTIONALITY
router.get('/gps/:id', function(req,res,next){
	var waypoints=[];
	var userLat;
	var userLen;
	Order.findById(req.params.id, function(err,result){
		for(var i=0; i<result.shoppingList.length; i++){
			console.log(result.shoppingList[i].lat);
			waypoints.push({lat: result.shoppingList[i].lat,
							lng:result.shoppingList[i].lng
						});
			console.log(waypoints);
		}
		userLat=result.userLat;
		userLen=result.userLng;
		console.log('Waypoints: '+waypoints);
		res.render('gps', {array:waypoints, userLat:userLat, userLen:userLen, orderID:req.params.id});
	});
	
});
//SHOW DRIVER
router.get('/showgps/:user', function(req,res,next)
{
	res.render('show', {id:req.params.user});
});
//RESULT ROUTE
router.get('/result/:id', function(req,res,next){
	Order.findById(req.params.id, function(err,result){
		console.log(result);
		res.render('result', {order:result, id:req.params.id});
	});
});
//AWAIT ROUTE
router.get('/await/:id', function(req,res,next){
	Order.findById(req.params.id, function(err,result){
		res.render('await', {sck:req.params.id, userID:result.userId});
	});
});
router.get('/rank/:id', function(req,res,next)
{
	user.findById(req.params.id, function(err,result){
		res.render('rank', {user:result,id:req.params.id});
	});
});
function isVerified(req,res,next)
{
	if(req.isAuthenticated())
	{
		user.findById(req.user, function(err,user){
			if(req.user.verified)
			{
				return next();
			}
			req.session.oldUrl=req.url;
			res.redirect('/checkout');
		});
	} else {
		req.session.oldUrl=req.url;
		res.redirect('/login');
	}
}

module.exports = router;
