var mongoose=require('mongoose').set('debug', true);
var Schema=mongoose.Schema;
var messageSchema=new Schema({
    from:String,
    to:String,
    title:String,
    content:String,
    date:Date,
    read:{type:Boolean, default:false}
});
module.exports = mongoose.model('message', messageSchema);