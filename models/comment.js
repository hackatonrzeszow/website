var mongoose=require('mongoose').set('debug', true);
var Schema=mongoose.Schema;
var commentSchema=new Schema({
    user: {type:String, required:true},
    commentedUser: {type:String, required:true},
    comment: {type:String, required:true}
});
module.exports = mongoose.model('comment', commentSchema);