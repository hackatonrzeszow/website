var mongoose=require('mongoose').set('debug', true);
var Schema=mongoose.Schema;
var userSchema = mongoose.Schema({
  google: {
    id: String,
    token: String,
    email: String,
    name: String,
    image: String
  },
  facebook: {
    id: String,
    token: String,
    email: String,
    name: String,
    username: String,
  },  
    starRating: {type: Number, default:0},
    ratingNum: {type:Number, default:0},
    verified: {type: Boolean, default:false},
    balance: {type: Number, default: 0}
});
module.exports=mongoose.model('User',userSchema);