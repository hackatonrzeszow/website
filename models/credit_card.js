var mongoose=require('mongoose');
var bcrypt=require('bcrypt-nodejs');
var Schema=mongoose.Schema;
var creditcard=new Schema({
	user: {type:String, required:true},
	token: {type: String, required:true}
});

module.exports = mongoose.model('CreditDetail', creditcard);