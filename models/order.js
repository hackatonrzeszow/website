var mongoose=require('mongoose').set('debug', true);
var Schema=mongoose.Schema;

var orderschema= new Schema({
	userId:{type:String, required:true},
	user:{type:String, required:true},
	userLat:{type:String, required:true},
	userLng:{type:String,required:true},
	shoppingList:[{}],
	cost:{type:Number, required:true},
	taken:{type:Boolean, default:false},
	takenBy:{type:String, required:false}
})
module.exports=mongoose.model('Order', orderschema);