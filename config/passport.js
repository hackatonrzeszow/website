var passport=require('passport');
var GoogleStrategy=require('passport-google-oauth20').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;  
var User=require('../models/user');
passport.serializeUser(function(user, done){
    done(null, user.id);
});
passport.deserializeUser(function(id, done){
    User.findById(id,function(err,user){
        done(err,user);
    });
});
passport.use(new FacebookStrategy({  
    clientID: '162574494276644',
    clientSecret: '8cb838fab2193c3fe0834bc1dbae21c7',
    callbackURL: 'http://projecthackathon.azurewebsites.net/auth/facebook/callback',
    profileFields: ['id', 'email', 'first_name', 'last_name', 'photos'],
  },
  function(token, refreshToken, profile, done) {
    process.nextTick(function() {
      User.findOne({ 'facebook.id': profile.id }, function(err, user) {
        if (err)
          return done(err);
        if (user) {
          return done(null, user);
        } else {
          var newUser = new User();
          newUser.facebook.id = profile.id;
          newUser.facebook.token = token;
          newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
          newUser.facebook.email = (profile.emails[0].value || '').toLowerCase();

          newUser.save(function(err) {
            if (err)
              throw err;
            return done(null, newUser);
          });
        }
      });
    });
  }));
passport.use(new GoogleStrategy({
	clientID: '1078473173558-500uii1ms6aoge48ei8pe8bts53m68or.apps.googleusercontent.com',
	clientSecret: 'm4voe_RWGcte_EYe13oNYcj3',
	callbackURL: "http://projecthackathon.azurewebsites.net/auth/google/callback",

},
 function(token, refreshToken, profile, done) {
      process.nextTick(function() {
        User.findOne({ 'google.id': profile.id }, function(err, user) {
          if (err)
            return done(err);
          if (user) {
            return done(null, user);
          } else {
            var newUser = new User();
            newUser.google.id = profile.id;
            newUser.google.token = token;
            newUser.google.name = profile.displayName;
            newUser.google.email = profile.emails[0].value;
            newUser.google.image = profile.photos[0].value;
            newUser.save(function(err) {
              if (err)
                throw err;
              return done(null, newUser);
            });
          }
        });
      });
    }));