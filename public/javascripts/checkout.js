Stripe.setPublishableKey('pk_test_g9Idv6z8R7yTcOwoigZbEYVB');

var $form = $('#checkout-form');

$form.submit(function (event) {
    $('#charge-error').addClass('hidden');
    $form.find('button').prop('disabled', true);
    Stripe.card.createToken({
        number: $('#cnum').val(),
        cvc: $('#cvc').val(),
        exp_month: $('#cardexpm').val(),
        exp_year: $('#cardexpy').val(),
        name: $('#card-name').val()
    }, stripeResponseHandler);
    return false;
});

function stripeResponseHandler(status, response) {
    if (response.error) { // Problem!

        // // Show the errors on the form
        // $('#charge-error').text(response.error.message);
        // $('#charge-error').removeClass('hidden');
        Materialize.toast(response.error.message, 5000, 'red');
        $form.find('button').prop('disabled', false); // Re-enable submission

        

    } else { // Token was created!

        // Get the token ID:
        var token = response.id;

        // Insert the token into the form so it gets submitted to the server:
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));

        // Submit the form:
        $form.get(0).submit();

    }
}